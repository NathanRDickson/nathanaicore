from setuptools import setup, find_packages

setup(
    name='NathanAICore',
    version='0.1.2',
    packages=find_packages(),
    license='Proprietary',
    description='The Core Toolbox for My AI Core Code',
    url='https://gitlab.com/NathanRDickson/nathanaicore',
    author='Nathan Dickson',
    author_email='NathanDickson@icloud.com'
)